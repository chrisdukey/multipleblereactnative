package com.activimeter.ble;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ClockPacket extends PdiPacket {

    public ClockPacket(byte[] _bytes, String deviceAddress) {
        super(BLE_CMD_GETRTC, _bytes);
        this.deviceAddress = deviceAddress;
        try {
            seconds = data.get();
            Log.e(DEBUGTAG, "Sec: " + seconds);
            minutes = data.get();
            Log.e(DEBUGTAG, "Min: " + minutes);
            hours = data.get();
            Log.e(DEBUGTAG, "Hrs: " + hours);
            day = data.get();
            Log.e(DEBUGTAG, "Day: " + day);
            data.get(); // We just want to skip this byte
            month = data.get();
            Log.e(DEBUGTAG, "Mth: " + month);
            year = data.get() + 2000;
            Log.e(DEBUGTAG, "Yr: " + year);

            dateTime = getDateTime();
        } catch (BufferUnderflowException ex) {
            Log.e(DEBUGTAG, "BufferUnderFlow  exception in ClockPacket");
        }
    }

    private static final String DEBUGTAG = "SP10C";

    String deviceAddress;
    int seconds;
    int minutes;
    int hours;
    int day;
    int month;
    int year;
    Calendar dateTime;

    public Calendar getDateTime() {
        return new GregorianCalendar(year, month , day, hours, minutes, seconds);
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    // Convert packet to JSON for return to Cordova
    public JSONObject toJSON() {
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        JSONObject json;
        try {
            json = new JSONObject(jsonString);
        } catch (JSONException ex) {
            Log.e(DEBUGTAG, "Failed to convert ClockPacket to JSONObject");
            json = null;
        }

        return json;
    }
}
