package com.activimeter.ble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

public class BluetoothLePlugin extends ReactContextBaseJavaModule implements ActivityEventListener {

    private static final String TAG = "BLEPlugin";

    //Client Configuration UUID for notifying/indicating
    private final UUID clientConfigurationDescriptorUuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public final static UUID UUID_SP_SERVICE = UUID.fromString("01000000-0000-0000-0000-000000000080");
    public final static UUID UUID_SP_WRITE_CHARACTERISTIC = UUID.fromString("04000000-0000-0000-0000-000000000080");

    private final boolean start_streaming = true;
    private final boolean stop_streaming = false;
    private final boolean on = true;
    private final boolean off = false;


    private BluetoothAdapter bluetoothAdapter;
    private ReactApplicationContext _reactContext;

    static final int ENABLE_REQUEST = 1;

    private Promise enableBluetoothPromise;

    // key is the MAC Address
    private Map<String, Peripheral> peripherals = new LinkedHashMap<>();

    // key is the name of the device e.g. SP-10C420
    private Map<String, Promise> scanPromises = new LinkedHashMap<>();

    public BluetoothLePlugin(ReactApplicationContext reactContext) {
        super(reactContext);
        _reactContext = reactContext;
    }


    public void unregisterReceivers() {
        getCurrentActivity().unregisterReceiver(mReceiver);
    }


    @ReactMethod
    public void start(Promise promise) {
        Log.d(TAG, "start");
        if (getBluetoothAdapter() == null) {
            Log.d(TAG, "No bluetooth support");
            promise.reject("REACT_START", "No bluetooth support");
            return;
        }
        _reactContext.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        promise.resolve(true);
        Log.d(TAG, "BLEPlugin initialized");
    }


    @Override
    public String getName() {
        return "RNMultipleBLE";
    }

    private String getLongUUID(String uuid) {
        if (uuid.length() == 4) {
            return "0000" + uuid + "-0000-1000-8000-00805f9b34fb";
        }
        return uuid;
    }


    private BluetoothManager getBluetoothManager() {
        return (BluetoothManager) getCurrentActivity().getSystemService(Context.BLUETOOTH_SERVICE);
    }


    private BluetoothDevice getDevice(String address) throws Exception {
        BluetoothManager bluetoothManager = (BluetoothManager) getCurrentActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothDevice device = bluetoothManager.getAdapter().getRemoteDevice(address);
        if (device == null) {
            throw new Exception("Unable to find device with address " + address);
        }
        return device;
    }


    @ReactMethod
    public void getAdapterState(Promise promise) throws JSONException {
        BluetoothManager bluetoothManager = (BluetoothManager) getCurrentActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        promise.resolve(ReactNativeJson.convertJsonToMap(JSONObjects.asAdapter(bluetoothManager.getAdapter())));
    }

    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            Log.d(TAG, "onScanResult");
            processResult(result);

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.d(TAG, "onBatchScanResults: " + results.size() + " results");
/*            for (ScanResult result : results) {
                processResult(result);
            }*/
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.w(TAG, "LE Scan Failed: " + errorCode);
        }

        private void processResult(final ScanResult result) {


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "New LE Device: " + result.getDevice().getName() + " @ " + result.getRssi());


                    String address = result.getDevice().getAddress();

                    if (!peripherals.containsKey(address)) {

                        Peripheral peripheral = new Peripheral(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes(), _reactContext);
                        peripherals.put(address, peripheral);

                    }

                    JSONObject obj = JSONObjects.asDevice(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());

                    try {
                        scanPromises.get(result.getDevice().getName()).resolve(ReactNativeJson.convertJsonToMap(obj));
                        Log.d(TAG, "processResult, map returned via Promise");
                    } catch (JSONException ex) {
                        scanPromises.get(result.getDevice().getName()).reject(ex);

                    } finally {
                        scanPromises.remove(result.getDevice().getName());
                    }

                    BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner().stopScan(leScanCallback);
                    raiseStopDiscoveryEvent();
                    //sendEvent(_reactContext, "leScanResult", obj);

                }
            });
        }
    };


    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable JSONObject obj) {

        if (obj != null) {
            //BundleJSONConverter bjc = new BundleJSONConverter();
            try {
                //Bundle bundle = bjc.convertToBundle(obj);
                //WritableMap map = Arguments.fromBundle(bundle);
                //sendEvent(_reactContext, eventName, map);

                sendEvent(_reactContext, eventName, ReactNativeJson.convertJsonToMap(obj));

            } catch (JSONException ex) {
                Log.e(TAG, "JSONException converting JSON to Map in sendEvent");
            }
        }
    }


    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }


    @ReactMethod
    public void startDiscovery(final String deviceName, final Integer SCAN_PERIOD, Promise promise) throws Exception {

        if (SCAN_PERIOD >= 0) {
            Log.d(TAG, "startDiscovery method called");
            Handler mHandler = new Handler();

            // Store the promise so the leScanCallback can return it
            scanPromises.put(deviceName, promise);

            if (SCAN_PERIOD > 0) {
                // Stop the scan after the period defined by the user
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopDiscovery();

                                // If the promise is still in the scanPromises Map then the device wasn't found
                                // during scanning so we return false
                                if (scanPromises.containsKey(deviceName)) {
                                    scanPromises.get(deviceName).resolve(false);
                                    scanPromises.remove(deviceName);
                                }
                            }
                        });


                    }
                }, SCAN_PERIOD);
            }

            // Filter the scan so we only get the device with the name we're looking for
            ScanFilter filter = new ScanFilter.Builder().setDeviceName(deviceName).build();
            List<ScanFilter> filterList = new ArrayList<ScanFilter>(1);
            filterList.add(filter);

            ScanSettings scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.CALLBACK_TYPE_FIRST_MATCH).build();


            BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner().startScan(filterList, scanSettings, leScanCallback);


        } else {
            promise.reject("COULD_NOT_START_SCAN", "Could not start scan (BluetoothAdapter#startLeScan). Scan period must be zero or positive integer.");
        }

    }

    @ReactMethod
    public void stopDiscovery() {
        Log.d(TAG, "stopDiscovery method called");
        // Ensure callback isn't null. can happen if adapter is turned off
        if (leScanCallback != null) {
            BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner().stopScan(leScanCallback);
            raiseStopDiscoveryEvent();
        }
    }

    private void raiseStopDiscoveryEvent() {
        // Let react native know that discover has stopped
        sendEvent(_reactContext, "discoveryStopped", JSONObjects.asString("discoveryStopped"));

    }

    @ReactMethod
    public void isConnected(String address, Promise promise) {
        promise.resolve(isConnected(address));
    }

    private boolean isConnected(String address) {
        boolean result = false;
        BluetoothManager bluetoothManager = getBluetoothManager();
        List<BluetoothDevice> devices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER);
        for (BluetoothDevice device : devices) {
            if (device.getAddress().equals(address)) {
                result = true;
            }
        }
        return result;
    }

    private BluetoothAdapter getBluetoothAdapter() {
        if (bluetoothAdapter == null) {
            BluetoothManager manager = (BluetoothManager) _reactContext.getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = manager.getAdapter();
        }
        return bluetoothAdapter;
    }

    @ReactMethod
    public void connect(String address, Promise promise) throws Exception {


        Log.d(TAG, "Connect to: " + address);

        Peripheral peripheral = peripherals.get(address);
        if (peripheral == null) {
            if (address != null) {
                address = address.toUpperCase();
            }
            if (getBluetoothAdapter().checkBluetoothAddress(address)) {
                BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
                peripheral = new Peripheral(device, _reactContext);
                peripherals.put(address, peripheral);
            } else {
                promise.reject("REACT_CONNECT", "Invalid peripheral uuid");
                promise.reject("REACT_CONNECT", "Invalid peripheral uuid");
                return;
            }
        }
        peripheral.connect(promise, getCurrentActivity());
    }

    @ReactMethod
    public void disconnect(final String address, Promise promise) throws Exception {

        Log.d(TAG, "Disconnect from: " + address);

        Peripheral peripheral = peripherals.get(address);
        if (peripheral != null) {
            peripheral.disconnect();
            promise.resolve(true);
        } else {
            promise.reject("REACT_DISCONNECT", "Peripheral not found");
        }


    }


    @ReactMethod
    public void getService(String address, String uuid, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {

            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(uuid));
            if (service == null) {
                promise.reject("NO_SERVICE_FOR_UUID", String.format("No service found with the given UUID %s", uuid));
            } else {
                promise.resolve(JSONObjects.asService(service, peripheral.getGatt().getDevice()));
            }
        } else {
            promise.reject("REACT_GETSERVICE", "Peripheral not found");
        }


    }

    @ReactMethod
    public void getServices(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {

            JSONArray result = new JSONArray();
            for (BluetoothGattService s : peripheral.getGatt().getServices()) {
                result.put(JSONObjects.asService(s, peripheral.getGatt().getDevice()));
            }
            promise.resolve(result);

        } else {
            promise.reject("REACT_GETSERVICES", "Peripheral not found");
        }


    }

    @ReactMethod
    public void getCharacteristic(String address, String serviceId, String characteristicId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            if (characteristic == null) {
                promise.reject("NO_CHAR_FOR_SERVICE", String.format("No characteristic found with the given Service ID %s", serviceId));
            } else {
                promise.resolve(JSONObjects.asCharacteristic(characteristic));
            }

        } else {
            promise.reject("REACT_GETCHARACTERISTIC", "Peripheral not found");
        }
    }

    @ReactMethod
    public void getCharacteristics(String address, String serviceId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            JSONArray result = new JSONArray();
            for (BluetoothGattCharacteristic c : service.getCharacteristics()) {
                result.put(JSONObjects.asCharacteristic(c));
            }
            promise.resolve(result);

        } else {
            promise.reject("REACT_GETCHARACTERISTICS", "Peripheral not found");
        }
    }

    @ReactMethod
    public void getIncludedServices(String address, String serviceId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            JSONArray result = new JSONArray();
            for (BluetoothGattService s : service.getIncludedServices()) {
                result.put(JSONObjects.asService(s, peripheral.getGatt().getDevice()));
            }
            promise.resolve(result);

        } else {
            promise.reject("REACT_GETINCLUDEDSERVICES", "Peripheral not found");
        }
    }

    @ReactMethod
    public void getDescriptor(String address, String serviceId, String characteristicId, String descriptorId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(descriptorId));

            if (descriptor == null) {
                promise.reject("NO_DESCRIPTOR_FOR_UUID", String.format("No characteristic found with the given Service ID %s", descriptorId));
            } else {
                promise.resolve(JSONObjects.asDescriptor(descriptor));
            }

        } else {
            promise.reject("REACT_GETDESCRIPTOR", "Peripheral not found");
        }
    }

    @ReactMethod
    public void getDescriptors(String address, String serviceId, String characteristicId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            JSONArray result = new JSONArray();
            for (BluetoothGattDescriptor d : characteristic.getDescriptors()) {
                result.put(JSONObjects.asDescriptor(d));
            }
            promise.resolve(result);

        } else {
            promise.reject("REACT_GETDESCRIPTORS", "Peripheral not found");
        }
    }

    @ReactMethod
    public void readCharacteristicValue(String address, String serviceId, String characteristicId, Promise promise) throws Exception {
        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));

            boolean result = peripheral.getGatt().readCharacteristic(characteristic);
            if (!result) {
                promise.reject("CHAR_READ_FAILED", String.format("Could not initiate characteristic read operation for %s", characteristicId));
            } else {
                promise.resolve(result);
            }

        } else {
            promise.reject("REACT_GETCHARACTERISTICVALUE", "Peripheral not found");
        }
    }

    @ReactMethod
    public void writeCharacteristicValue(String address, String serviceId, String characteristicId, String strValue, Promise promise) throws Exception {

        byte[] value = strValue.getBytes();

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));

            boolean result = characteristic.setValue(value);

            if (!result) {
                promise.reject("CHAR_WRITE_FAILED", String.format("Could not store characteristic value for %s", characteristicId));
                return;
            }

            if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0) {
                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            } else if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0) {
                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            }

            result = peripheral.getGatt().writeCharacteristic(characteristic);

            if (!result) {
                promise.reject("CHAR_WRITE_FAILED", String.format("Could not initiate characteristic write operation for %s", characteristicId));
            } else {
                promise.resolve(result);
            }

        } else {
            promise.reject("REACT_WRITECHARACTERISTICVALUE", "Peripheral not found");
        }

    }

    @ReactMethod
    public void startCharacteristicNotifications(String address, String serviceId, String characteristicId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(clientConfigurationDescriptorUuid);

            boolean result = false;

            result = peripheral.getGatt().setCharacteristicNotification(characteristic, true);

            if (!result) {
                promise.reject("START_CHAR_NOTIFICATIONS_FAILED", String.format("Could not set enable notifications/indications for %s", characteristicId));
                return;
            }

            if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {
                result = descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            } else if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0) {
                result = descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
            } else {
                promise.reject("START_CHAR_NOTIFICATIONS_FAILED", String.format("The device does not support notifications/indications on the given characteristic: %s", characteristicId));
                return;
            }

            if (!result) {
                peripheral.getGatt().setCharacteristicNotification(characteristic, false);
                promise.reject("START_CHAR_NOTIFICATIONS_FAILED", String.format("Could not store notification/indication value on the characteristic's configuration descriptor: %s", characteristicId));
                return;
            }


            if (!result) {
                peripheral.getGatt().setCharacteristicNotification(characteristic, false);
                promise.reject("START_CHAR_NOTIFICATIONS_FAILED", String.format("Could not initiate writing the notification/indication value on the characteristic's configuration descriptor: %s", characteristicId));
            } else {
                promise.resolve(result);
            }

        } else {
            promise.reject("REACT_STARTCHARACTERISTICNOTIFICATIONS", "Peripheral not found");
        }

    }

    @ReactMethod
    public void stopCharacteristicNotifications(String address, String serviceId, String characteristicId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(clientConfigurationDescriptorUuid);

            boolean result = false;

            result = peripheral.getGatt().setCharacteristicNotification(characteristic, false);

            if (!result) {
                promise.reject("STOP_CHAR_NOTIFICATIONS_FAILED", String.format("Could not disable notifications/indications: %s", characteristicId));
                return;
            }

            if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {
                result = descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            } else if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0) {
                // yes, supposedly disabling "indications" is done the same as disabling "notifications"
                result = descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            } else {
                promise.reject("STOP_CHAR_NOTIFICATIONS_FAILED", String.format("The device does not support notifications/indications on the given characteristic: %s", characteristicId));
                return;
            }

            if (!result) {
                promise.reject("STOP_CHAR_NOTIFICATIONS_FAILED", String.format("Could not store notification/indication value on the characteristic's configuration descriptor: %s", characteristicId));
                return;
            }

            result = peripheral.getGatt().writeDescriptor(descriptor);

            if (!result) {
                promise.reject("STOP_CHAR_NOTIFICATIONS_FAILED", String.format("Could not initiate writing the notification/indication value on the characteristic's configuration descriptor: %s", characteristicId));
            } else {
                promise.resolve(result);
            }
        } else {
            promise.reject("REACT_STOPCHARACTERISTICNOTIFICATIONS", "Peripheral not found");
        }

    }

    @ReactMethod
    public void readDescriptorValue(String address, String serviceId, String characteristicId, String descriptorId, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(descriptorId));


            boolean result = peripheral.getGatt().readDescriptor(descriptor);

            if (!result) {
                promise.reject("READ_DESC_VALUE", String.format("Could not initiate reading descriptor: %s", descriptorId));
            } else {
                promise.resolve(result);
            }
        } else {
            promise.reject("REACT_READDESCRIPTORVALUE", "Peripheral not found");
        }
    }

    @ReactMethod
    public void writeDescriptorValue(String address, String serviceId, String characteristicId, String descriptorId, String strValue, Promise promise) throws Exception {

        byte[] value = strValue.getBytes();

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            BluetoothGattService service = peripheral.getGatt().getService(UUID.fromString(serviceId));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicId));
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(descriptorId));

            boolean result = descriptor.setValue(value);

            if (!result) {
                promise.reject("WRITE_DESC_VALUE", String.format("Could not store descriptor value: %s", descriptorId));
                return;
            }

            result = peripheral.getGatt().writeDescriptor(descriptor);

            if (!result) {
                promise.reject("WRITE_DESC_VALUE", String.format("Failed on BluetoothGatt#writeDescriptor for descriptor %s. Android didn't tell us why either.", descriptorId));
            } else {
                promise.resolve(result);
            }
        } else {
            promise.reject("REACT_WRITEDESCRIPTORVALUE", "Peripheral not found");
        }
    }

    @ReactMethod
    public void controlStreaming(String address, boolean enable, Promise promise) {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.controlStreaming(enable);
            promise.resolve(true);
        } else {
            promise.reject("REACT_CONTROLSTREAMING", "Peripheral not found");
        }

    }

    @ReactMethod
    public void controlStreamingPeriod(String address, int streamingPeriodMs, Promise promise) {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.controlStreaming(streamingPeriodMs);
            promise.resolve(true);
        } else {
            promise.reject("REACT_CONTROLSTREAMING", "Peripheral not found");
        }

    }



    @ReactMethod
    public void deviceGetStatus(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.deviceGetStatus();
            promise.resolve(true);
        } else {
            promise.reject("REACT_DEVICEGETSTATUS", "Peripheral not found");
        }

    }

    @ReactMethod
    public void setRealtimeClock(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.setRTC();
            promise.resolve(true);
        } else {
            promise.reject("REACT_SETREALTIMECLOCK", "Peripheral not found");
        }
    }

    @ReactMethod
    public void getRealtimeClock(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.getRTC();
            promise.resolve(true);
        } else {
            promise.reject("REACT_GETREALTIMECLOCK", "Peripheral not found");
        }
    }

    @ReactMethod
    public void loggerGetStatus(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.loggerGetStatus();
            promise.resolve(true);
        } else {
            promise.reject("REACT_LOGGERGETSTATUS", "Peripheral not found");
        }
    }

    @ReactMethod
    public void loggerGetRecords(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.loggerGetRecords();
            promise.resolve(true);
        } else {
            promise.reject("REACT_LOGGERGETRECORDS", "Peripheral not found");
        }
    }

    /**
     * @custom.ReactMethod {@code clearLog(String address)}
     */
    @ReactMethod
    public void clearLog(String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.clearLog();
            promise.resolve(true);
        } else {
            promise.reject("REACT_CLEARLOG", "Peripheral not found");
        }
    }

    @ReactMethod
    public void controlLogging(Boolean enable, String address, Promise promise) throws Exception {

        Peripheral peripheral = peripherals.get(address);

        if (peripheral != null) {
            peripheral.controlLogging(enable);
            promise.resolve(true);
        } else {
            promise.reject("REACT_CONTROLLOGGING", "Peripheral not found");
        }
    }

    @ReactMethod
    public void enableBluetooth(Promise promise) {
        if (getBluetoothAdapter() == null) {
            Log.d(TAG, "No bluetooth support");
            promise.reject("REACT_ENABLEBLUETOOTH", "No bluetooth support");
            return;
        }
        if (!getBluetoothAdapter().isEnabled()) {
            enableBluetoothPromise = promise;
            Intent intentEnable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            getCurrentActivity().startActivityForResult(intentEnable, ENABLE_REQUEST);
        } else
            promise.resolve(true);

    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            getCurrentActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                        int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                        // Returns the state of the adapter when it's turned off or on
                        if (state == BluetoothAdapter.STATE_OFF || state == BluetoothAdapter.STATE_ON) {
                            sendEvent(_reactContext, "BLEStateChanged", JSONObjects.asAdapter(getBluetoothManager().getAdapter()));
                        }
                    } else if (intent.getAction().equals(BluetoothDevice.ACTION_FOUND)) {
                        // Get the BluetoothDevice object from the Intent
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                        Log.d(TAG, "device found " + JSONObjects.asDevice(device, rssi, device.getAddress().getBytes()));
                        sendEvent(_reactContext, "deviceFound", JSONObjects.asDevice(device, rssi, device.getAddress().getBytes()));
                    }
                }
            });
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (requestCode == ENABLE_REQUEST && enableBluetoothPromise != null) {
            if (resultCode == RESULT_OK) {
                enableBluetoothPromise.resolve(true);
            } else {
                enableBluetoothPromise.reject("onActivityResult", "User refused to enable bluetooth");
            }
            enableBluetoothPromise = null;
        }
    }
}
