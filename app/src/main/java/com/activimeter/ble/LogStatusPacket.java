package com.activimeter.ble;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class LogStatusPacket extends PdiPacket {


    public LogStatusPacket(byte[] _bytes, String deviceAddress) {
        super(PDI_CMD_LOGSTATUS, _bytes);
        this.deviceAddress = deviceAddress;
        enabled = data.get();
        dummy = data.get();
        log_NumOfRecords = data.getShort() & 0xFFFF; // Convert to unsigned
        log_UsedBytes = data.getInt();
        log_TotalBytes = data.getInt();

    }
    private static final String DEBUGTAG = "SP10C";
    String deviceAddress;

    // 0 is off, 1 is Logging, 2 is Erasing
    byte enabled;
    byte dummy; // Spare
    int log_NumOfRecords;
    int log_UsedBytes;
    int log_TotalBytes;


    public byte getEnabled() {
        return enabled;
    }

    public int getLog_NumOfRecords() {
        return log_NumOfRecords;
    }

    public int getLog_UsedBytes() {
        return log_UsedBytes;
    }

    public int getLog_TotalBytes() {
        return log_TotalBytes;
    }

    // Convert packet to JSON for return to Cordova
    public JSONObject toJSON()
    {
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        JSONObject json;
        try{
            json = new JSONObject(jsonString);
        }
        catch (JSONException ex){
            Log.e(DEBUGTAG, "Failed to convert LogStatusPacket to JSONObject");
            json = null;
        }

        return json;
    }
}
